// List of items
items = [
    {
        name: "Handphone 1",
        price: 399,
        image: 'images/img1.png',
        category: 'Electronics'
    },
    {
        name: "Handphone 2",
        price: 459,
        image: 'images/img2.png',
        category: 'Electronics'
    },
    {
        name: "Handphone 3",
        price: 249,
        image: 'images/img3.png',
        category: 'Electronics'
    },
    {
        name: "Handphone 4",
        price: 379,
        image: 'images/img4.png',
        category: 'Electronics'
    },
    {
        name: "Handphone 5",
        price: 799,
        image: 'images/img5.png',
        category: 'Electronics'
    },
    {
        name: "Sepatu 1",
        price: 99,
        image: 'images/img6.png',
        category: 'Clothes'
    },
    {
        name: "Sepatu 2",
        price: 89,
        image: 'images/img7.png',
        category: 'Clothes'
    },
    {
        name: "Sepatu 3",
        price: 129,
        image: 'images/img8.png',
        category: 'Clothes'
    },
    {
        name: "Sepatu 4",
        price: 79,
        image: 'images/img9.png',
        category: 'Clothes'
    },
    {
        name: "Sepatu 5",
        price: 99,
        image: 'images/img10.png',
        category: 'Clothes'
    }
];
// Jangan Edit Code di Bagian Atas Ini
// Tulis kode kalian di bawah ini

// Membuat function yang akan digunakan
// clearDisplay function: menghapus semua product yang ada di dalam display
const clearDisplay = () => {
    const container = document.querySelector("#display-container-inner");
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
}

// displayProduct function: menampilkan product (yang telah difilter) 
const displayProduct = (products) => {
    // <div class="product">
    //     <!-- Product Image -->
    //     <img src="images/img1.png" alt="product image" class="product-image">
    //     <!-- Product Name -->
    //     <h3 class="product-name">Product 1</h3>
    //     <!-- Product Price -->
    //     <p class="product-pricing">$100</p>
    // </div>
    clearDisplay();
    const container = document.querySelector("#display-container-inner");

    products.forEach(product => {
        const name = product.name;
        const price = product.price;
        const imageUrl = product.image;

        const newProduct = document.createElement('div');
        newProduct.classList.add("product");

        const newProductImage = document.createElement("img");
        newProductImage.src = imageUrl;
        newProductImage.alt = name;
        newProductImage.classList.add("product-image");

        const newProductName = document.createElement("h3");
        newProductName.classList.add("product-name");
        newProductName.innerText = name;

        const newProductPrice = document.createElement("p");
        newProductPrice.classList.add("product-price");
        newProductPrice.innerText = `$${price}`;

        newProduct.appendChild(newProductImage);
        newProduct.appendChild(newProductName);
        newProduct.appendChild(newProductPrice);

        container.appendChild(newProduct);
    });
}

// filterProduct function: memfilter product berdasarkan category yang dipilih
const filterProduct = (products, category) => {
    const categoryLabel = document.querySelector("#category");
    categoryLabel.innerText = category;

    if (category === "All") return products;
    return products.filter(product => product.category === category);
}

// searchProduct function: mencari product berdasarkan keyword yang diinput
const searchProduct = (products, keyword) => {
    return products.filter(product => product.name.toLowerCase().includes(keyword.trim().toLowerCase()));
}


// Bagian DOM
// Memilih HTML element yang akan digunakan
const categoryButtons = document.querySelectorAll(".category-button");

const allCategoryButton = categoryButtons[0];
const electronicsCategoryButton = categoryButtons[1];
const clothesCategoryButton = categoryButtons[2];

const searchBar = document.querySelector("#search-input");
console.log(searchBar);

// Menambahkan event listener pada element-element yang ada
allCategoryButton.addEventListener("click", () => {
    displayProduct(filterProduct(items, "All"));
})

electronicsCategoryButton.addEventListener("click", () => {
    displayProduct(filterProduct(items, "Electronics"))
})

clothesCategoryButton.addEventListener("click", () => {
    displayProduct(filterProduct(items, "Clothes"))
})

searchBar.addEventListener("change", () => {
    console.log(searchBar.value);
    displayProduct(searchProduct(items, searchBar.value))
})

// Tampilkan seluruh product untuk pertama kalinya
displayProduct(items);